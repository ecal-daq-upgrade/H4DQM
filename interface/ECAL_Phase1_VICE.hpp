#ifndef ECAL_PHASE1_VICE_H
#define ECAL_PHASE1_VICE_H

#include "interface/StandardIncludes.hpp"
#include "interface/Board.hpp"
#include "interface/Event.hpp"

class ECAL_Phase1_VICE: public Board
{

public :

    ECAL_Phase1_VICE() {};

    int Unpack(dataType &stream) { return -1; }
    int Unpack(dataType &stream, Event * event, boardHeader&);

    int headNSamples(uint32_t header)  { return  header & 0x3FFF;    }
    int headFrequency(uint32_t header) { return 40 * (((header>>14) & 0x3) + 1);  }
    int headNDevices(uint32_t header)  { return (header>>16) & 0x7;  }
    int headFwVersion(uint32_t header) { return (header>>19) & 0x1F; }

private :
    std::array<float, 4> gain_map_; 
};

#endif
