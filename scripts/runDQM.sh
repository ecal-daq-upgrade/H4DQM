#!/bin/bash

#export LD_LIBRARY_PATH=/home/cmsdaq/DAQ/H4Analysis/lib:/home/cmsdaq/DAQ/H4Analysis/DynamicTTree/lib/:/home/cmsdaq/DAQ/H4Analysis/CfgManager/lib/:$LD_LIBRARY_PATH

input="/tmp/"
output="/tmp"
run="0"
spill="0"
prescale=1
keepUnpack=0
unpackOnly=0
clean=0
noPlots=0
replay=0

TEMP=`getopt -o cnki:o:r:s:p:u:w:a: --long clean,unpackOnly,keepUnpack,unpackFolder,input:,output:,run:,spill:,unprescaledSpills:,prescale:,webDQM,noPlots,replay -n 'runDQM.sh' -- "$@"`
if [ $? != 0 ] ; then echo "Options are wrong..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"

while true; do
    case "$1" in
	-i | --input ) input="$2"; shift 2 ;;
	-o | --output ) output="$2"; shift 2 ;;
	-u | --unpackFolder ) unpackFolder="$2"; shift 2 ;;
	-a | --unprescaledSpills ) unprescaledSpills="$2"; shift 2 ;;
	-r | --run ) run="$2"; shift 2;;
	-s | --spill ) spill="$2"; shift 2;;
	-p | --prescale ) prescale=$2; shift 2;;
	-k | --keepUnpack ) keepUnpack=1; shift 1;;
	-c | --clean ) clean=1; shift 1;;
	-w | --webDQM ) webDQM=$2; shift 2;;
	-n | --unpackOnly ) unpackOnly=1; shift 1;;
	--noPlots ) noPlots=1; shift 1;;
	--replay ) replay=1; shift;;
	-- ) shift; break ;;
	* ) break ;;
    esac
done

###---Run DQM replay of a run
if [ $replay -eq 1 ]; then
    spills=`ls -la $input/$run | grep raw | awk '{print $9}' | sed 's:.raw::g'`
    rm -r /data/public_DQM_plots/$run/
    for rspill in $spills
    do	
	set -x
	/home/cmsdaq/DAQ/H4DQM/scripts/runDQM.sh -i ${input} -o ${output} -u ${unpackFolder} -r ${run} -s ${rspill} -p ${prescale} -k -w ${webDQM}
	set +x
    done
    exit 0
fi

mkdir -p $unpackFolder
mkdir -p /tmp/unpack/$run/
mkdir -p /tmp/h4reco/$run/
echo "UNPACK => run: $run spill: $spill"
/home/cmsdaq/DAQ/H4DQM/bin/unpack -i $input  -o $unpackFolder -r $run -s $spill > /tmp/unpack/$run/${spill}.log 2>&1 	    

if [ $unpackOnly -eq 1 ]; then
    exit 0
fi

###---Run H4Analysis reco
mkdir -p /data/ntuples/$run/
echo "RECO => run: $run spill: $spill"
set -x
/home/cmsdaq/DAQ/H4Analysis/bin/H4Reco /home/cmsdaq/DAQ/H4Analysis/cfg/ECAL_b892/ECAL_b892_reco.cfg $run $spill > /tmp/h4reco/$run/${spill}.log
set +x

###---Run DQM plots
if [ $noPlots -ne 1 ]; then
    spill=$(echo $spill | sed 's/^0*//')
    export run=$run
    export spill=$spill
    if [ $spill -eq 1 ]; then
        let prev_spill=$spill-1
    elif [ $spill -le $prescale ]; then
        let prev_spill=$spill-$prescale+1
    else
        let prev_spill=$spill-$prescale
    fi
    export prev_spill=$prev_spill  
    export outdir=/data/public_DQM_plots/$run/$spill/

    #parallel '/home/cmsdaq/DAQ/H4Analysis/FuriousPlotter/draw.py -c {} -p "current_spill /data/ntuples/${run}/${spill}.root h4","prev.spill /data/public_DQM_plots/${run}/${prev_spill}/","draw.outDir ${outdir}"' ::: `cat /home/cmsdaq/DAQ/H4DQM/data/current_dqm_plots.txt`

    echo "DQM => run: $run spill: $spill"
    set -x
    /home/cmsdaq/DAQ/FuriousPlotter/draw.py -f -c /home/cmsdaq/DAQ/H4DQM/cfg/ECAL_b892_dqm_base.cfg -p "spill_src /data/ntuples/${run}/${spill}.root h4","draw.outDir ${outdir}" > /tmp/h4reco/$run/${spill}_plot_base.log 2>&1
    set +x

    if [ ! -d "${outdir}/../merged/" ]; then
    	mkdir -p ${outdir}/../merged/
    	rsync -aP ${outdir}/* ${outdir}/../merged/
    	echo `ls ${outdir}/../merged/ | grep '.png\|.txt\|.root\|.pdf'`
    	for file in `ls ${outdir}/../merged/ | grep '.png\|.txt\|.root\|.pdf'`
    	do
    	    unlink ${outdir}/../$file
    	    ln -s ${outdir}/../merged/$file ${outdir}/../$file
    	done
    	rsync -aP /home/cmsdaq/DAQ/H4DQM/skel_DQM/ /data/public_DQM_plots/$run/merged/
    else
    	echo "DQM MERGER => run: $run spill: $spill"
    	set -x
    	/home/cmsdaq/DAQ/FuriousPlotter/draw.py -f -c /home/cmsdaq/DAQ/H4DQM/cfg/ECAL_b892_dqm_append.cfg -p "current.spill ${outdir}","prev.spill ${outdir}/../merged/","draw.outDir /tmp/DQM/${run}/${spill}/" > /tmp/h4reco/$run/${spill}_plot_append.log 2>&1
    	set +x
    	mv /tmp/DQM/${run}/${spill}/* ${outdir}/../merged/
    fi

    rsync -aP /home/cmsdaq/DAQ/H4DQM/skel_DQM/ /data/public_DQM_plots/$run/
    rsync -aP /home/cmsdaq/DAQ/H4DQM/skel_DQM/ /data/public_DQM_plots/$run/$spill
    
    # if [ "$webDQM" != "localhost" ] ; then
    # 	rsync -aP /data/public_DQM_plots/$run/ $webDQM:/data/public_DQM_plots/$run/
    # fi
fi
