#!/usr/bin/env python3

import sys
import os
from copy import deepcopy
from datetime import datetime
import subprocess
import argparse
import functools
import shutil
import random
import string

from influxdb import InfluxDBClient
import urllib3
urllib3.disable_warnings()

dbname = 'ecal_h4'
dbhost = 'ecal-automation-relay.web.cern.ch'
dbport = 80
dbusr = 'ecalgit'
dbpwd = 'ecalPbWO'
dbssl = False

def processing_states():
    """
    Valid processing states
    """

    return ['new',
            'transfer-run',
            'transfer-failed',
            'transfer-done',
            'unpacker-run',
            'unpacker-failed',
            'unpacker-done'
        ]

def set_status(point, status):
    """
    Set a coherent status
    """

    point['fields'].update({state : 0 for state in processing_states()})
    point['fields'][status] = 1

def db_data_template():
    """
    Returns the tempalte for data to be inserted in the influxdb
    """

    ### besic schema
    data = {
        'measurement' : 'spill_file',
        'tags' : {
            'campaign' : None,
            'run' : None,
            'spill' : None
        },
        'time' : None,
        'fields' : {
            'eb-machine' : '',
            'eb-path' : '',
            'unpacked-path' : ''
        }
    }  

    ### add a fields for each processing state
    data['fields'].update({state : 0 for state in processing_states()})

    return data

def reformat_query_data(data):
    """
    Reformat data from query to be a list of data ready to be inserted in the db
    """

    points = []

    if len(data.items()) > 0:
        for d in data.items()[-1][-1]:
            points.append(db_data_template())
            d = reformat_last(d)
            for key in ['tags', 'fields']:
                points[-1][key].update((k, d[k]) for k in set(points[-1][key]).intersection(d))

    return points

def reformat_last(data):
    """
    Remove the automatically prepended 'last_' to results from queries involving last()
    """
    
    # copy keys to avoid 'dictionary changed during loop' error
    keys = list(data.keys())
    for key in keys:
        data[key.replace('last_', '')] = data.pop(key)
    
    return data

def validate_db_data(data):
    """
    Check that the data to be inserted in the db are valid
    """

    ### file can only be in 1 state of processing
    check = functools.reduce(lambda x, state: x + data['fields'][state] if state in processing_states() else x, data['fields'], 0)  

    return check

def status(opts, db):
    """
    Update spill status
    """

    prev_data = db.query('SELECT last(*) FROM "%s" WHERE "campaign" = \'%s\' AND "run" = \'%s\' AND "spill" = \'%s\'' % 
                    ('spill_file', opts.campaign, opts.run, opts.spill))

    data = db_data_template()

    ### set time stamp
    data['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    
    ### set the file tags
    data['tags']['run'] = opts.run
    data['tags']['spill'] = opts.spill
    data['tags']['campaign'] = opts.campaign
    
    prev_data = next(prev_data.get_points(), None)
    if prev_data != None:
        prev_data = reformat_last(prev_data) 
        # if status is already correctly set, skip update
        r = functools.reduce(lambda x, sts: x & (prev_data[sts] == int(getattr(opts, sts))), processing_states(), True)
        if r:
            return 0
    ### set the file fields
    for sts in processing_states():
        data['fields'][sts] = int(getattr(opts, sts))
    data['fields']['eb-machine'] = opts.eb_machine if opts.eb_machine else prev_data['eb-machine']
    data['fields']['eb-path'] = os.path.abspath(opts.eos_path+'/'+opts.campaign+'/EB/'+opts.run+'/{:04d}.raw'.format(int(opts.spill))) if opts.eos_path else prev_data['eb-path']
    data['fields']['unpacked-path'] = os.path.abspath(opts.eos_path+'/'+opts.campaign+'/DataTree/'+opts.run+'/{:04d}.root'.format(int(opts.spill))) if opts.eos_path else prev_data['unpacked-path']
    
    check = validate_db_data(data)
    if check == 1:
        print(data)
        db.write_points([data])
    else:
        return -1

    return 0

def transfer(opts, db):
    """
    Transfer data from the DAQ machine
    """

    ### Get all spill in status new
    if not opts.run:
        db_data = db.query('SELECT * FROM (SELECT last(*) FROM "%s" WHERE "campaign" = \'%s\' GROUP BY "run", "spill") WHERE "last_new" = 1' % 
                           ('spill_file', opts.campaign))        
    ### If run or run+spill are specified force re-transfer
    elif not opts.spill:
        db_data = db.query('SELECT * FROM "%s" WHERE "campaign" = \'%s\' AND "new" = 1 AND "run" = \'%s\' ' % 
                           ('spill_file', opts.campaign, opts.run))
    else:
        db_data = db.query('SELECT * FROM "%s" WHERE "campaign" = \'%s\' AND "new" = 1 AND "run" = \'%s\' AND "spill" = \'%s\'' %                         
                           ('spill_file', opts.campaign, opts.run, opts.spill))
    
    ### Copy files marked as new from local EB machine to EOS
    ### - the raw data files path on the local machine is assumed to be /data/raw/EB/
    cache = []
    cache_t0 = datetime.utcnow()
    for spill in reformat_query_data(db_data):
        ### campaign is not returned by the influx query since it is used as filter
        os.makedirs(os.path.dirname(spill['fields']['eb-path']), exist_ok=True)
        shutil.copy2(os.path.abspath('/data/raw/EB/{:d}/{:04d}.raw'.format(int(spill['tags']['run']), int(spill['tags']['spill']))), 
                     os.path.abspath(spill['fields']['eb-path']))
        ### reformat info to push new status to influx (if copy is successful)
        if os.path.exists(spill['fields']['eb-path']) and os.path.isfile(spill['fields']['eb-path']):
            spill['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            set_status(spill, 'transfer-done')
            check = validate_db_data(spill)
            if check == 1:
                print(spill)
                cache.append(spill)        
            if (datetime.utcnow()-cache_t0).seconds > 60:
                db.write_points(cache)
                cache = []
                cache_t0 = datetime.utcnow()
                     
    ### make sure to write remaining spills
    db.write_points(cache)

    return 0

def get_proxy():
    """
    Return location of the x509 user proxy
    """

    out = subprocess.run(['voms-proxy-info', '-e', '--valid', '1:00'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if out.returncode:
        raise Exception('voms proxy not found or validity  less than 1 hours:\n%s' % out)
    out = subprocess.run(['voms-proxy-info', '-p'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if out.returncode:
        raise Exception('Unable to voms proxy:\n%s' % out)
    proxy = out.stdout.decode('utf-8').strip('\n')

    return proxy

def submit_unpack(htcmap, queue='espresso'):
    """
    Submit unpacker jobs for cached run+spills with htcondor
    """

    suffix = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))
    os.makedirs('unpacker_jobs_%s' % suffix)
    with open('unpacker_jobs_%s/runspill_submit_list.txt' % suffix, 'w') as flist:
        flist.write('\n'.join('%s, %s, %s' % rs for rs in htcmap))
    with open('unpacker_jobs_%s/unpacker_job.sh' % suffix, 'w') as fsh:
        fsh.write(
"""#!/bin/bash
source ~/.bashrc
conda activate h4analysis
h4autoctrl.py unpack -c ${1} -r ${2} -s ${3}
"""
        )
    args = (queue, ) + (suffix, )*4 + (get_proxy(), suffix) 
    with open('unpacker_jobs_%s/unpacker_job.sub' % suffix, 'w') as fsub:
        fsub.write(
"""+JobFlavour = %s

executable = unpacker_jobs_%s/unpacker_job.sh
arguments = $(campaign) $(run) $(spill)
output = unpacker_jobs_%s/unpack.$(ClusterId).$(ProcId).out
error  = unpacker_jobs_%s/unpack.$(ClusterId).$(ProcId).err
log    = unpacker_jobs_%s/unpack.$(ClusterId).log
x509userproxy = %s
max_retries = 2

queue campaign, run, spill from unpacker_jobs_%s/runspill_submit_list.txt
""" % args
        )
    
    ret = subprocess.run(['condor_submit', 'unpacker_jobs_%s/unpacker_job.sub' % suffix], 
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    
    print(ret.stdout)

    return ret.returncode

def unpack(opts, db):
    """
    Unpack raw data
    """

    ### Get all spill in status transfer-done
    if not opts.run:
        db_data = db.query('SELECT * FROM (SELECT last(*) FROM "%s" WHERE "campaign" = \'%s\' GROUP BY "run", "spill") WHERE "last_transfer-done" = 1 OR "last_unpacker-failed" = 1' % 
                           ('spill_file', opts.campaign))
    ### If run or run+spill are specified force re-unpack
    elif not opts.spill:
        db_data = db.query('SELECT * FROM "%s" WHERE "campaign" = \'%s\' AND "transfer-done" = 1 AND "run" = \'%s\' ' % 
                           ('spill_file', opts.campaign, opts.run))
        print('SELECT * FROM "%s" WHERE "campaign" = \'%s\' AND "transfer-done" = 1 AND "run" = \'%s\' ' % 
              ('spill_file', opts.campaign, opts.run), db_data)
    else:
        db_data = db.query('SELECT * FROM "%s" WHERE "campaign" = \'%s\' AND "transfer-done" = 1 AND "run" = \'%s\' AND "spill" = \'%s\'' % 
                           ('spill_file', opts.campaign, opts.run, opts.spill))

    cache = []
    htcmap = []
    cache_t0 = datetime.utcnow()
    for spill in reformat_query_data(db_data):
        spill['tags']['campaign'] = opts.campaign
        if opts.batch and opts.spill == None:
            htcmap.append((spill['tags']['campaign'], spill['tags']['run'], spill['tags']['spill']))
            spill['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            set_status(spill, 'unpacker-run')
            check = validate_db_data(spill)
            if check == 1:
                cache.append(spill)        
        else:
            ### run the unpacker on a single spill
            os.makedirs(os.path.dirname(spill['fields']['unpacked-path']), exist_ok=True)
            unpack_ret = subprocess.run(['unpack', 
                                         '-i', os.path.abspath(os.path.dirname(spill['fields']['eb-path'])+'/../'),
                                         '-o', os.path.abspath(os.path.dirname(spill['fields']['unpacked-path'])+'/../'),
                                         '-r', spill['tags']['run'],
                                         '-s', spill['tags']['spill']], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)                
            ### reformat info to push new status to influx (if unpacker ran successfully)
            sts = unpack_ret.returncode == 0 and os.path.exists(spill['fields']['unpacked-path']) and os.path.isfile(spill['fields']['unpacked-path']) 
            set_status(spill, 'unpacker-done' if sts else 'unpacker-failed')
            spill['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
            check = validate_db_data(spill)
            if check == 1:
                cache.append(spill)        

        ### upload cache to the db
        if (datetime.utcnow()-cache_t0).seconds > 60 or len(cache)>500:
            db.write_points(cache)
            cache = []
            cache_t0 = datetime.utcnow()
            if opts.batch and len(htcmap) > 500:
                if submit_unpack(htcmap, queue=opts.batch) == 0:
                    htcmap = []

    ### make sure to write remaining spills
    db.write_points(cache)
    if opts.batch and len(htcmap) > 0:
        submit_unpack(htcmap, queue=opts.batch)
    
    return 0

### dictionary of available subcommands
subcommands = {
    'status' : status,
    'transfer'  : transfer,
    'unpack' : unpack
}


def cmdline_options():
    """
    Returns the argparse instance to handle the cmdline options for this script (needed to easily generate 
    the docs using sphinx-argparse)
    """

    parser = argparse.ArgumentParser(description=
    """
    Upload a new spill to the ECAL H4 influxdb

    """, formatter_class=argparse.RawTextHelpFormatter)
    
    ### sub commands     
    subparsers = parser.add_subparsers(dest='subcommand', description='Select the DB operation')

    ### status control subcommand
    status_parser = subparsers.add_parser('status', help='Update job status in the DB')
    status_parser.add_argument('-r', '--run', dest='run', help='H4DAQ run number', required=True)    
    status_parser.add_argument('-s', '--spill', dest='spill', help='H4DAQ spill number', required=True)    
    status_parser.add_argument('-c', '--campaign', dest='campaign', help='H4DAQ campaign', required=True)    
    status_parser.add_argument('--eos-path', dest='eos_path', help='EOS path to store the data')    
    status_parser.add_argument('--eb-machine', dest='eb_machine', help='H4DAQ machine hosting the raw data (usually the eventbuilder or drcv machine)') 

    group = status_parser.add_mutually_exclusive_group(required=True )
    for sts in processing_states():
        group.add_argument('--'+sts, dest=sts, action='store_true')

    ### transfer control subcommand
    transfer_parser = subparsers.add_parser('transfer', help='Transfer spill in status new data. If run [and spill] are speicified, force re-transfer')
    transfer_parser.add_argument('-c', '--campaign', dest='campaign', help='H4DAQ campaign', required=True)    
    transfer_parser.add_argument('-r', '--run', dest='run', default=None, help='H4DAQ run number')    
    transfer_parser.add_argument('-s', '--spill', dest='spill', default=None, help='H4DAQ spill number')    

    ### unpack control subcommand
    unpack_parser = subparsers.add_parser('unpack', help='Unpack spill in status new data. If run [and spill] are speicified, force re-unpack')
    unpack_parser.add_argument('-c', '--campaign', dest='campaign', help='H4DAQ campaign', required=True)    
    unpack_parser.add_argument('-r', '--run', dest='run', default=None, help='H4DAQ run number')    
    unpack_parser.add_argument('-s', '--spill', dest='spill', default=None, help='H4DAQ spill number')    
    unpack_parser.add_argument('--batch', dest='batch', default=None, help='Send unpack jobs to condor (the option value will be used as the job Flavour)')

    return parser

if __name__ == '__main__':
    """
    This script is designed to collect H4DAQ raw data and create and entry in the influxdb database.
    """    

    opts = cmdline_options().parse_args()

    db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)
    ret = subcommands[opts.subcommand](opts, db)

    sys.exit(ret)
