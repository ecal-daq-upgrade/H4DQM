#!/usr/bin/env python3

import os, sys
import argparse
import glob
import time
import subprocess
import shutil

def unpack(args, spill=None):
    if spill is None: spill = args.spill
    if args.log_files:
        os.makedirs(f'/tmp/unpack/{args.run}/', exist_ok = True)
    os.makedirs(f'{args.unpack}', exist_ok = True)
    print(f'UNPACK => run: {args.run} spill: {spill}')
    command = f'{args.daq_base}/H4DQM/bin/unpack \
    -i {args.input} \
    -o {args.unpack} \
    -r {args.run} \
    -s {spill}'
    print(command)
    log = f'/tmp/unpack/{args.run}/{spill}.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.run(command, shell=True, stdout=flog, stderr=subprocess.PIPE).returncode
    print(f'Log saved in {log}')
    if args.unpackOnly:
        sys.exit(0)

def reco(args, spill = None):
    if spill is None: spill = args.spill
    retries = 0
    while not os.path.isfile(os.path.abspath(args.unpack+'/'+args.run+'/'+spill+'.root')):
        time.sleep(30)
        unpack(args)
        # give up
        retries += 1
        if retries > 5:
            sys.exit(-1)

    nspill = spill.lstrip("0")
    if int(nspill)%int(args.prescale) != 0 and int(nspill) != 1:
        return

    if args.log_files:
        os.makedirs(f'/tmp/h4reco/{args.run}/', exist_ok = True)
    os.makedirs(f'{args.reco}/{args.run}', exist_ok = True)
    
    print(f'RECO => run: {args.run} spill: {spill}')
    command = f'{args.daq_base}/H4Analysis/bin/H4Reco\
    {args.cfg_reco}\
    {args.run}\
    {args.spill}'
    print(command)
    log = f'/tmp/h4reco/{args.run}/{spill}.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.call(command, shell=True, stdout=flog, stderr=subprocess.PIPE)
    print(f'Log saved in {log}')
    
def dqm_plots(args, spill = None):
    if spill is None: spill = args.spill
    
    spill = spill.lstrip("0")
    if int(spill)%int(args.prescale) != 0 and int(spill) != 1:
        return

    if int(spill) == 1:
        prev_spill = int(spill) - 1
    elif int(spill) <= args.prescale:
        prev_spill = int(spill) - args.prescale + 1
    else:
        prev_spill = int(spill) - args.prescale

    # if os.path.isdir(f'{args.dqm}/{args.run}'):
    #     print(f'{args.dqm}/{args.run} already exists')
    #     sys.exit(0)
    outdir = os.path.abspath(f'{args.dqm}/{args.run}/{spill}')
    os.makedirs(outdir, exist_ok=True)
    os.system(f'rsync -aP {args.daq_base}/H4DQM/skel_DQM/ {args.dqm}/{args.run}/')
    os.system(f'rsync -aP {args.daq_base}/H4DQM/skel_DQM/ {args.dqm}/{args.run}/{spill}')
    print(f'DQM => run: {args.run} spill: {spill}')
    command = f'{args.daq_base}/FuriousPlotter/draw.py \
    -c {args.cfg_plot} \
    -p \'spill_src {args.reco}/{args.run}/{spill}.root h4\',\'draw.outDir {outdir}\''
    print(command)
    log = f'/tmp/h4reco/{args.run}/{spill}_plot_base.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.call(command, shell=True, stdout=flog, stderr=subprocess.PIPE)
    if not os.path.isdir(f'{outdir}/../merged') and int(spill)==1:
        os.makedirs(f'{outdir}/../merged', exist_ok=True)
        os.system(f'rsync -aP {args.daq_base}/H4DQM/skel_DQM/ {args.dqm}/{args.run}/merged/')
        subprocess.call(f'cp -r {outdir}/* {outdir}/../merged/', shell = True)
        ext = ['*.txt', '*.png', '*.root', '*.pdf']
        files = []
        for e in ext:
            files.extend(glob.glob(f'{outdir}/../merged/{e}'))
        for f in files:
            subprocess.call('ln -s '+os.path.relpath(f, os.path.abspath(f'{args.dqm}/{args.run}'))+' '+os.path.abspath(f.replace('merged/', '')), shell = True)

    else:
        print(f'DQM MERGER=> run: {args.run} spill: {spill}')
        command = f'{args.daq_base}/FuriousPlotter/draw.py \
        -c {args.cfg_merge} \
        -p "current.spill {outdir},prev.spill {outdir}/../merged/,draw.outDir {args.dqm}/{args.run}/{spill}/"'
        print(command)
        p = subprocess.call(command, shell=True, stdout=flog, stderr=subprocess.PIPE)
        os.system(f'cp {args.dqm}/{args.run}/{spill}/* {outdir}/../merged/')
    if not args.webDQM == 'localhost':
        os.system(f'rsync -aP {args.dqm}/{args.run}/ {args.webDQM}:{args.dqm}/{args.run}/')


def replay_spill(args, spill):
    unpack(args, spill)
    reco(args, spill)
    dqm_plots(args, spill)

    
def main(arguments):
    sep = ', '
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--input', '-i', type=str, default = '/data/raw/EB/')
    parser.add_argument('--unpack', '-u', type=str, default = '/data/raw/DataTree/', help = 'unpacked file output directory')
    parser.add_argument('--reco', '-e', type=str, default = '/data/ntuples/', help = 'reco file output directory')
    parser.add_argument('--dqm', '-d', type=str, default = '/data/public_DQM_plots/', help = 'dqm file output directory')
    parser.add_argument('--run', '-r', type=str, default ='0')
    parser.add_argument('--cfg-reco', type=str, default = '/home/cmsdaq/DAQ/H4Analysis/cfg/ECAL_H4_July2021/ECAL_H4_base.cfg')
    parser.add_argument('--cfg-plot', type=str, default = '/home/cmsdaq/DAQ/H4DQM/cfg/ECAL_H4_base.cfg')
    parser.add_argument('--cfg-merge', type=str, default = '/home/cmsdaq/DAQ/H4DQM/cfg/ECAL_H4_append.cfg')
    parser.add_argument('--daq-base', type=str, default = '/home/cmsdaq/DAQ/')    
    parser.add_argument('--spill', '-s', type=str, default ='0')
    parser.add_argument('--prescale', '-p', type=int, default = 1)
    parser.add_argument('--webDQM', '-w', type=str, default = 'localhost')
    parser.add_argument('--unpackOnly', '-n', default = False, action = 'store_true')
    parser.add_argument('--no-plots', default = False, action = 'store_true')
    parser.add_argument('--replay', default = False, action = 'store_true')
    parser.add_argument('--log-files', default = False, action = 'store_true', help = 'Use log files instead of stdout')

    args = parser.parse_args(arguments)

    run = args.run
    
    if (args.replay):
        spills = glob.glob(f'{input}/{run}/*raw*')
        spills = [rspill.replace('.raw', '') for rspill in spills]
        os.system(f'rm -rf {args.dqm}/{run}')
        for rspill in spills:
            replay_spill(args, rspill)

        
    unpack(args)
    if not args.unpackOnly:
        reco(args)
        if not args.no_plots:
            dqm_plots(args)
    
if __name__ == '__main__':
    main(sys.argv[1:])
