#!/usr/bin/env python3

import os, sys
import argparse
import glob
import time
import subprocess
import shutil


def check_unpacked(args):
    spills = []
    raw_files = glob.glob(f'{args.input}/{args.run}/*')
    unpacked_files = glob.glob(f'{args.unpack}/{args.run}/*')
    raw_files = [f.split("/")[-1].replace('.raw','') for f in raw_files]
    unpacked_files = [f.split("/")[-1].replace('.root','') for f in unpacked_files]
    spills = [f for f in raw_files if not f in unpacked_files]

    return spills

def unpack(args, spill):
    if args.log_files:
        os.makedirs(f'/tmp/unpack/{args.run}/', exist_ok = True)
    os.makedirs(f'{args.unpack}', exist_ok = True)
    print(f'UNPACK => run: {args.run} spill: {spill}')
    command = f'{args.daq_base}/H4DQM/bin/unpack \
    -i {args.input} \
    -o {args.unpack} \
    -r {args.run} \
    -s {spill}'
    print(command)
    log = f'/tmp/unpack/{args.run}/{spill}.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.run(command, shell=True, stdout=flog, stderr=subprocess.PIPE).returncode
    print(f'Log saved in {log}')

def reco(args):
    if args.log_files:
        os.makedirs(f'/tmp/h4reco/{args.run}/', exist_ok = True)
    os.makedirs(f'{args.reco}/{args.run}', exist_ok = True)
    
    print(f'RECO => run: {args.run}')
    command = f'{args.daq_base}/H4Analysis/bin/H4Reco\
    {args.cfg_reco}\
    {args.run}'
    print(command)
    log = f'/tmp/h4reco/{args.run}.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.call(command, shell=True, stdout=flog, stderr=subprocess.PIPE)
    print(f'Log saved in {log}')
    
def dqm_plots(args):
    outdir = os.path.abspath(f'{args.dqm}/{args.run}')
    os.makedirs(outdir, exist_ok=True)
    os.system(f'rsync -aP {args.daq_base}/H4DQM/skel_DQM/ {args.dqm}/{args.run}/')
    print(f'DQM => run: {args.run}')
    command = f'{args.daq_base}/FuriousPlotter/draw.py \
    -c {args.cfg_plot} \
    -p \'spill_src {args.reco}/{args.run}.root h4\',\'draw.outDir {outdir}\''
    print(command)
    log = f'/tmp/h4reco/{args.run}_plot_base.log'
    flog = open(log, "w") if args.log_files else None
    p = subprocess.call(command, shell=True, stdout=flog, stderr=subprocess.PIPE)
    if not args.webDQM == 'localhost':
        os.system(f'rsync -aP --ignore-existing {args.dqm}/{args.run}/ {args.webDQM}:{args.dqm}/{args.run}/')


def main(arguments):
    sep = ', '
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--input', '-i', type=str, default = '/data/raw/EB/')
    parser.add_argument('--unpack', '-u', type=str, default = '/data/raw/DataTree/', help = 'unpacked file output directory')
    parser.add_argument('--reco', '-e', type=str, default = '/data/ntuples/', help = 'reco file output directory')
    parser.add_argument('--dqm', '-d', type=str, default = '/data/public_DQM_plots/', help = 'dqm file output directory')
    parser.add_argument('--run', '-r', type=str, default ='0')
    parser.add_argument('--spill', '-s', type=str, default = None)
    parser.add_argument('--cfg-reco', type=str, default = '/home/cmsdaq/DAQ/H4Analysis/cfg/ECAL_H4_July2021/ECAL_H4_base.cfg')
    parser.add_argument('--cfg-plot', type=str, default = '/home/cmsdaq/DAQ/H4DQM/cfg/ECAL_H4_base.cfg')
    parser.add_argument('--cfg-merge', type=str, default = '/home/cmsdaq/DAQ/H4DQM/cfg/ECAL_H4_append.cfg')
    parser.add_argument('--daq-base', type=str, default = '/home/cmsdaq/DAQ/')    
    parser.add_argument('--prescale', '-p', type=int, default = 1)
    parser.add_argument('--webDQM', '-w', type=str, default = 'localhost')
    parser.add_argument('--unpackOnly', '-n', default = False, action = 'store_true')
    parser.add_argument('--log-files', default = False, action = 'store_true', help = 'Use log files instead of stdout')

    args = parser.parse_args(arguments)

    run = args.run

    if not args.spill:
        spills = check_unpacked(args)
    else:
        spills = [args.spill]
    
    for s in spills:    
        unpack(args, s)

    if not args.unpackOnly:
        if len(spills): reco(args)
        for s in spills:
            dqm_plots(args)
    
if __name__ == '__main__':
    main(sys.argv[1:])
